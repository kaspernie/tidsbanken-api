package com.experis.tidsbankenapi.Controllers;

import com.experis.tidsbankenapi.Models.dbo.Ineligible;
import com.experis.tidsbankenapi.Repositories.IIneligibleRepository;
import com.experis.tidsbankenapi.Repositories.IUserRepository;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin("${server.cors.application_origin}")
@SecurityRequirement(name = "keycloak_implicit")
@RequestMapping("api")
public class IneligibleController {

    private final IIneligibleRepository ineligibleRepository;
    private final IUserRepository userRepository;

    public IneligibleController(IIneligibleRepository ineligibleRepository, IUserRepository userRepository) {
        this.ineligibleRepository = ineligibleRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/ineligible")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<List<Ineligible>> getIneligible(@AuthenticationPrincipal Jwt principal){
        Optional<List<Ineligible>> searchResult = ineligibleRepository.OrderByPeriodStartAsc();
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(searchResult.get());
    }

    @PostMapping("/ineligible")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<String> createIneligiblePeriod(@RequestBody Ineligible newIneligible, @Parameter(hidden=true) @AuthenticationPrincipal Jwt principal){
        Ineligible createdIneligible = new Ineligible();
        createdIneligible.setPeriodStart(newIneligible.getPeriodStart());
        createdIneligible.setPeriodEnd(newIneligible.getPeriodEnd());
        createdIneligible.setUser(userRepository.getUserById(UUID.fromString(principal.getSubject())));
        ineligibleRepository.save(createdIneligible);
        return new ResponseEntity<>("Ineligible period created", HttpStatus.CREATED);
    }

    @GetMapping("/ineligible/{ineligibleId}")
    @PreAuthorize("hasAnyRole({'user','admin'})")
    public ResponseEntity<Ineligible> getIneligibleById(@PathVariable Integer ineligibleId){
        Optional<Ineligible> searchResult = ineligibleRepository.findById(ineligibleId);
        if(searchResult.isEmpty()){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(searchResult.get());
    }

    @DeleteMapping("/ineligible/{ineligibleId}")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<Boolean> deleteIneligibleById(@PathVariable Integer ineligibleId) {
        ineligibleRepository.deleteById(ineligibleId);
        return ResponseEntity.ok(true);
    }


    @PatchMapping("/ineligible/{ineligibleId}")
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<Ineligible> patchIpById(@PathVariable Integer ineligibleId, @RequestBody Ineligible patchIneligible) {
        Ineligible ineligible = ineligibleRepository.getIneligibleById(ineligibleId);
        if (patchIneligible.getPeriodStart() != null) {
            ineligible.setPeriodStart(patchIneligible.getPeriodStart());
        }
        if (patchIneligible.getPeriodEnd() != null) {
            ineligible.setPeriodEnd(patchIneligible.getPeriodEnd());
        }
        ineligibleRepository.save(ineligible);
        return ResponseEntity.ok(ineligibleRepository.getIneligibleById(ineligibleId));
    }
}


