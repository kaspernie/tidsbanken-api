package com.experis.tidsbankenapi.Repositories;

import com.experis.tidsbankenapi.Models.dbo.Ineligible;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IIneligibleRepository extends JpaRepository<Ineligible,Integer> {
    Optional<List<Ineligible>> OrderByPeriodStartAsc();
    Ineligible getIneligibleById(Integer ineligibleId);
}
