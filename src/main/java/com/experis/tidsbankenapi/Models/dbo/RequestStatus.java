package com.experis.tidsbankenapi.Models.dbo;

public enum RequestStatus {
    PENDING,
    APPROVED,
    DENIED;
}
