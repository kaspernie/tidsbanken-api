package com.experis.tidsbankenapi.Models.dbo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Comment extends NotificationContent{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 255)
    private String message;

    @Column
    private Date timeStamp;

    @Column
    private Date editedAtTime;

    @Column
    private Boolean isEdited;

    @Column(nullable = false)
    private Boolean seen;

    @JsonGetter(value = "user")
    public String getCommentUser(){
        if(user == null) return null;
        return String.format("/api/user/%s", user.getId());
    }

    @JsonGetter(value = "request")
    public String getCommentRequest(){
        if(request == null) return null;
        return String.format("/api/request/%d", request.getId());
    }

    @ManyToOne
    @JoinColumn(name = "fk_request_id")
    private Request request;

    @ManyToOne
    @JoinColumn(name = "fk_user_id")
    private User user;

    public Integer getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Request getRequest() {return request;}

    public void setRequest(Request request) {
        this.request = request;
    }

    public Date getEditedAtTime() {
        return editedAtTime;
    }

    public void setEditedAtTime(Date editedAtTime) {
        this.editedAtTime = editedAtTime;
    }

    public void setEdited(Boolean edited) {
        isEdited = edited;
    }


    public Boolean isSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }
}
