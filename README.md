# TIDSBANKEN API
This Spring Boot with Security application entails a PostgresSQL database made with JPA & Hibernate and exposed through a web API with the use of Swagger. All endpoints are OAUTH2.0 protected and uses Keycloak as identity provider.
The database and API is used with a single page web application that simulates a vacation request calendar.

## Installation

### Heroku
The API is online at [heroku-hosted-api](https://tidsbanken-api.herokuapp.com/swagger-ui/index.html)
All relevant documentation on the endpoints are at this location. Only users with correct Authorization and Authenticantion are allowed to access the endpoints.
## Usage
Single page web application is deployed at [heroku-hosted-app](https://tidsbanken.herokuapp.com)

## Contributing
Pull requests are welcome. For any changes, please open an issue first to discuss what you would like to change.

## Maintainers

[Jakob Henriksen (@jako3417)](https://gitlab.com/jakobah37)

[Kasper Nielsen (@kaspernie)](https://gitlab.com/kaspernie)

[Kasper Pedersen (@KPed)](https://gitlab.com/KPed)

[Nikolaj Nielsen (@Nsknielsen)](https://gitlab.com/Nsknielsen)


## License

[MIT](https://choosealicense.com/licenses/mit/)

